# ESP32-DEVKITC_V4
## 介绍
ESP32-DEVKITC_V4是乐鑫一款基于ESP32_WROVER_IE的小型开发板。单模组采用了ESP32系列的ESP32-D0WD-V3芯片（xtensa 32-bit LX6 双核处理器），CPU时钟频率可调节为80MHz到240MHz，配置4MB SPI flash和8MB SPI PSRAM，集成传统蓝牙、低功耗蓝牙和Wi-Fi。可以用于低功耗传感器网络和要求极高的任务，例如语音编码、音频流和 MP3 解码等。

开发板正面图片：

![ESP32正面照片](https://gitee.com/baochenjin/device_board_espressif/raw/master/figures/ESP32正面照片.jpg)

开发板背面图片：

![ESP32背面照片](https://gitee.com/baochenjin/device_board_espressif/raw/master/figures/ESP32背面照片.jpg)

开发板功能块图：

![ESP32-WROVER_IE功能图](https://gitee.com/baochenjin/device_board_espressif/raw/master/figures/ESP32-WROVER_IE功能图.png)


## 模组规格

|  器件类别	|  开发板|  
|  ----  | ----  | 
|  CPU	|  Xtensa 32-bit LX6 双核处理器（最高240MHz） |  
|  SRAM	|  520 KB|  
|  ROM	|  448 KB|
|  RTC SRAM	|  16 KB|
|  GPIO	|  34|  
|  I2C	|  2|  
|  UART |  3|  
|  SPI	|  4|
|  I2S	|  2|
|  8bit DAC |  2|
|  12-bit SAR ADC | 18channel 12bit |
|  touch sensors  | 10|
|  host (SD/eMMC/SDIO)  | 1|
|  Ethernet MAC  | IEEE-802.3-2008|
|  TWAI  | 兼容 ISO11898-1（CAN 规范 2.0）|
|  RMT (TX/RX) | 支持|
|  Motor PWM | 支持|
|  LED PWM | 支持|
|  Hall sensor | 支持|
|  Radio | 2.4 GHz 接收/发射器|
|  WiFi  | TCP/IP、802.11 b/g/n、DCF|
|  Bluetooth | Bluetoothv4.2、Class-1、Class-2、Class-3、SDIO/SPI/UART HCI

## 引脚定义

![ESP32-WROVER_IE管脚](https://gitee.com/baochenjin/device_board_espressif/raw/master/figures/ESP32-WROVER_IE管脚.png)

![ESP32-WROVER_IE管脚定义2](https://gitee.com/baochenjin/device_board_espressif/raw/master/figures/ESP32-WROVER_IE管脚定义2.png)


## 搭建开发环境

### 系统要求
系统要求基于Xtensa LX6结构的liteos_m内核操作系统，采用乐鑫官方提供的xtensa-esp32-elf-gcc 8.2.0版本的toolchain。

OpenHarmony需要按照官方文档介绍安装环境https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md，然后编译出烧录包，按照文档介绍烧录。

### 工具要求
ubuntu 18.04编译，windows10系统烧录。

https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md

### 搭建过程

https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md

## 编译调试

https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md

按下开发板上reset power按钮，通过串口调试工具打印串口log。

## 首个示例

代码默认有一个UART打印示例。

## 参考资源

https://gitee.com/openharmony-sig/device_soc_espressif/blob/master/README.md

https://www.espressif.com/zh-hans/support/documents/technical-documents?keys=&field_type_tid%5B%5D=681
